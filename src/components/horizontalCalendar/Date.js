import React, { PureComponent } from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { colors } from '../../common'

export default class Date extends PureComponent {
  // Style helper functions that merge active date styles with the default ones
  // when rendering a date that was selected by user or was set active by default

  getContainerStyle = () => ({
    ...styles.container,
    ...(this.props.isActive ? styles.containerActive : {})
  })

  getDayStyle = () => ({
    ...styles.text,
    ...styles.day,
    ...(this.props.isActive ? styles.textActive : {})
  })

  getDateStyle = () => ({
    ...styles.text,
    ...styles.date,
    ...(this.props.isActive ? styles.textActive : {})
  })

  // Call `onRender` and pass component's with when rendered
  onLayout = (event: {
    nativeEvent: {
      layout: { x: number, y: number, width: number, height: number }
    }
  }) => {
    const { index, onRender } = this.props
    const {
      nativeEvent: {
        layout: { width }
      }
    } = event
    onRender(index, width)
  }

  // Call `onPress` passed from the parent component when date is pressed
  onPress = () => {
    const { index, onPress } = this.props
    onPress(index)
  }

  render() {
    const { date } = this.props
    return (
      <TouchableOpacity
        style={this.getContainerStyle()}
        onLayout={this.onLayout}
        onPress={this.onPress}
      >
        <Text style={this.getDateStyle()}>{date.format('DD')}</Text>
        <Text style={this.getDayStyle()}>
          {date.format('ddd').toUpperCase()}
        </Text>
      </TouchableOpacity>
    )
  }
}

const styles = {
  container: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderBottomColor: 'transparent',
    borderBottomWidth: 2,
    paddingHorizontal: 2,
    paddingVertical: 2
  },
  containerActive: {
    backgroundColor: colors.accentLight
    // borderBottomColor: colors.accent
    // borderBottomColor: '#FFFFFF'
  },
  day: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 14
  },
  date: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 22
  },
  text: {
    color: colors.secondary,
    textAlign: 'center'
  },
  textActive: {
    color: colors.primary
  }
}
