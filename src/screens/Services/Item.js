import React, { Component } from 'react'
import { connect } from 'react-redux'
import { select } from '@rematch/select'
import {
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Text,
  StyleSheet
} from 'react-native'
import { width as fullWidth, colors } from '../../common'

class _Item extends Component {
  state = { width: 0, height: 0 }

  componentDidMount() {
    const { imageUrl } = this.props.service
    const { width, height } = this.state
    if (!width || !height)
      Image.getSize(imageUrl, (width, height) =>
        this.setState({ width, height })
      )
  }

  getMinutes = time => {
    try {
      const timeParts = time.split(':').map(item => Number(item))
      return 60 * timeParts[0] + timeParts[1]
    } catch (err) {
      return time
    }
  }

  onPress = () => {
    const { service, onPress } = this.props
    onPress(service.id)
  }

  render() {
    const { imageUrl, name, description, price, duration } = this.props.service
    const { width, height } = this.state
    return (
      <ImageBackground
        source={{ uri: imageUrl }}
        style={[
          styles.container,
          {
            width: 0.9 * fullWidth,
            height: (0.9 * fullWidth * height) / width
          }
        ]}
      >
        <TouchableOpacity
          onPress={this.onPress}
          style={{ flex: 1, justifyContent: 'flex-end' }}
          activeOpacity={0.8}
        >
          <View style={styles.textContainer}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                borderBottomColor: colors.secondary,
                borderBottomWidth: 1,
                paddingHorizontal: 5,
                paddingVertical: 10
              }}
            >
              <View style={{ flex: 0.75 }}>
                <Text style={styles.nameText}>{name}</Text>
              </View>
              <View style={{ flex: 0.25, alignItems: 'flex-end' }}>
                <Text style={styles.nameText}>{`${price}р`}</Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingHorizontal: 10,
                paddingVertical: 10
              }}
            >
              <View style={{ flex: 0.75 }}>
                <Text style={styles.descriptionText}>{description}</Text>
              </View>
              <View style={{ flex: 0.25, alignItems: 'flex-end' }}>
                <Text style={styles.durationText}>{`${this.getMinutes(
                  duration
                )} мин`}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center'
  },
  textContainer: {
    backgroundColor: 'rgba(176, 156, 123, 0.3)'
  },
  nameText: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 24,
    color: colors.secondary
  },
  descriptionText: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 16,
    color: 'rgba(255, 255, 255, 0.7)'
  },
  durationText: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 16,
    color: 'rgba(176, 156, 123, 1)'
  }
})

const mapState = (state, { id }) => ({
  service: select.services.getItem(state, id)
})

export const Item = connect(mapState)(_Item)
