import React, { Component } from 'react'
import { View, Image, TouchableOpacity, StatusBar } from 'react-native'
import { width, headerHeight, proportions } from '../../common'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'

class _Logo extends Component {
  onPressMenu = () => {
    const { signOut, navigation } = this.props
    signOut()
    navigation.navigate('AuthLoading')
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: StatusBar.currentHeight
        }}
      >
        <TouchableOpacity
          style={{ width: '10%', alignItems: 'flex-start', paddingLeft: 5 }}
          onPress={this.onPressMenu}
        >
          <Image
            style={{ width: 25, height: 25 }}
            source={{ uri: 'icon-menu' }}
          />
        </TouchableOpacity>
        <Image
          source={{ uri: 'logo' }}
          style={{
            width: (0.7 * headerHeight) / proportions.logo,
            height: 0.7 * headerHeight
          }}
        />
        <View style={{ width: '10%', paddingRight: 5 }} />
      </View>
    )
  }
}

mapDispatch = dispatch => ({
  signOut: dispatch.auth.signOut
})

export const Logo = withNavigation(
  connect(
    null,
    mapDispatch
  )(_Logo)
)
