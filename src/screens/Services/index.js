import React, { Component } from 'react'
import { connect } from 'react-redux'
import { select } from '@rematch/select'
import {
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  FlatList,
  StyleSheet
} from 'react-native'
import { Logo } from './Logo'
import { colors } from '../../common'
import { Item } from './Item'
import { StackActions } from 'react-navigation'

class _Services extends Component {
  static navigationOptions = {
    headerTransparent: true,
    headerTitle: <Logo />
  }

  async componentDidMount() {
    const { servicesList = [], load } = this.props
    if (!servicesList || servicesList.length === 0) {
      const response = await fetch('http://176.31.32.73:8001/api/Job/GetAll', {
        method: 'get'
      })
      if (response.ok) {
        const data = await response.json()
        load(data)
      }
    }
  }

  _keyExtractor = (id, index) => id
  _onPressItem = id => {
    this.props.setSelectedService(id)
    const pushAction = StackActions.push({
      routeName: 'Order',
      params: {
        serviceId: id
      }
    })
    this.props.navigation.dispatch(pushAction)
  }
  _renderItem = item => <Item id={item.item} onPress={this._onPressItem} />
  _itemSeparatorComponent = () => <View style={{ height: 20 }} />

  render() {
    const { servicesList = [] } = this.props
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={'transparent'} translucent />
        <ImageBackground
          source={require('../../../resources/images/barbershop.png')}
          style={styles.background}
          resizeMode="cover"
          opacity={0.5}
        >
          <Image
            source={require('../../../resources/images/gradient.png')}
            style={styles.salonInfoGradient}
            resizeMode="stretch"
          />
          <View style={styles.salonInfoContainer}>
            <Text style={styles.salon}>САЛОН</Text>
            <Text style={styles.name}>«Ваш стиль»</Text>
            <Text style={styles.address}>ул.Матросова 25 д.8</Text>
          </View>
        </ImageBackground>
        <View style={styles.underline} />
        <View style={styles.serviceTitleContainer}>
          <Text style={styles.servicesTitle}>УСЛУГИ САЛОНА</Text>
        </View>
        <FlatList
          data={servicesList}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._itemSeparatorComponent}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.primary
  },
  background: {
    width: '100%',
    height: '50%'
  },
  salonInfoGradient: {
    width: '80%',
    height: '60%',
    position: 'absolute',
    left: 0,
    bottom: 0
  },
  salonInfoContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    paddingLeft: 20,
    paddingBottom: 10
  },
  salon: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 24,
    color: colors.accentLight
    // backgroundColor: colors.primary
  },
  name: {
    fontFamily: 'Pattaya-Regular',
    fontSize: 30,
    color: colors.secondary
    // backgroundColor: colors.primary
  },
  address: {
    fontFamily: 'KellySlab-Regular',
    fontSize: 24,
    color: colors.secondary
    // backgroundColor: colors.primary
  },
  serviceTitleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10
  },
  servicesTitle: {
    fontFamily: 'KellySlab-Regular',
    fontSize: 24,
    color: colors.accentLight
  },
  underline: {
    width: '70%',
    height: 2,
    backgroundColor: colors.accentLight,
    alignSelf: 'flex-end'
  }
})

const mapState = state => ({
  servicesList: select.services.getAll(state)
})

const mapDispatch = dispatch => ({
  load: dispatch.services.load,
  setSelectedService: dispatch.selected.setSelectedService
})

export const Services = connect(
  mapState,
  mapDispatch
)(_Services)
