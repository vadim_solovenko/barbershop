import React, { Component } from 'react'
import {
  View,
  Text,
  StatusBar,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { select } from '@rematch/select'
import { colors, getMinutes } from '../../common'
import { DatePicker } from './DatePicker'
import { Time } from './Time'
import { Service } from './Service'
import { HorizontalCalendar } from '../../components'
import moment from 'moment'

class _Order extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props)
    const date = moment()
    const initServiceId = props.navigation.state.params.serviceId
    this.state = {
      initServiceId,
      date,
      serviceId: initServiceId,
      time: '',
      times: []
    }
  }

  static getDerivedStateFromProps(props, state) {
    const initServiceId = props.navigation.state.params.serviceId
    if (initServiceId !== state.initServiceId)
      return { ...state, initServiceId }
    else return null
  }

  componentDidMount = async () => {
    const { serviceId, date } = this.state
    this.setState({ times: await this.getShedule(serviceId, date) })
  }

  getShedule = async (serviceId, date) => {
    const body = JSON.stringify({
      serviceId,
      date
    })
    const response = await fetch(
      'http://176.31.32.73:8001/api/Job/GetSchedule',
      {
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'post',
        body
      }
    )
    if (response.ok) {
      const a = await response.json()
      return await a.times
    } else return []
  }

  sendOrder = async () => {
    const { serviceId, date: momentDate, time } = this.state
    const date = momentDate.toDate()
    const timeParts = time.split(':').map(item => Number(item))
    if (time !== '') {
      const dateWithTime = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDay(),
        timeParts[0],
        timeParts[1] || 0,
        0
      )
      const body = JSON.stringify({
        serviceId,
        date: dateWithTime
      })
      const response = await fetch(
        'http://176.31.32.73:8001/api/Order/ToOrder',
        {
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'post',
          body
        }
      )
      if (response.ok) {
        console.log('true')
      } else console.log('false')
    }
  }

  changeDate = async value => {
    this.setState(
      {
        date: value,
        time: '',
        times: await this.getShedule(this.state.serviceId, value)
      },
      () => console.log(this.state)
    )
  }

  changeService = async value => {
    this.setState({
      serviceId: value,
      time: '',
      times: await this.getShedule(value, this.state.date)
    })
  }

  changeTime = value => this.setState({ time: value })

  onGoBack = () => {
    this.props.navigation.goBack()
  }

  _GoBack = () => (
    <TouchableOpacity onPress={this.onGoBack}>
      <Image source={{ uri: 'icon_back' }} style={{ width: 25, height: 25 }} />
    </TouchableOpacity>
  )

  render() {
    const { time, times, serviceId } = this.state
    const { services, initService } = this.props
    const currentService = services[this.state.serviceId]
    return (
      <ImageBackground
        source={require('../../../resources/images/barbershop.png')}
        style={styles.background}
        resizeMode="stretch"
        opacity={0.15}
      >
        <ScrollView>
          {/* <DatePicker /> */}
          <HorizontalCalendar
            currentDate={this.state.date}
            onSelectDate={this.changeDate}
            LeftComponent={this._GoBack}
            // onGoBack={this.onGoBack}
            showDaysBeforeCurrent={15}
            showDaysAfterCurrent={15}
          />
          <View style={{ marginTop: 40 }}>
            <Service
              onChange={this.changeService}
              initId={initService}
              id={serviceId}
            />
          </View>
          <View style={{ alignItems: 'center', padding: 20 }}>
            <Text style={styles.textName}>{currentService.name}</Text>
            <Text style={styles.textPrice}>{`${getMinutes(
              currentService.duration
            )} минут / ${currentService.price} рублей`}</Text>
          </View>
          <Time
            selected={time}
            times={times}
            onChange={this.changeTime}
            sendOrder={this.sendOrder}
          />
        </ScrollView>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.secondary
  },
  textName: {
    fontFamily: 'KellySlab-Regular',
    fontSize: 24,
    color: colors.primary
  },
  textPrice: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 20,
    color: colors.primaryLight
  }
})

const mapState = state => ({
  services: state.services,
  initService: select.selected.getService(state)
})

export const Order = connect(mapState)(_Order)
