import React, { Component } from 'react'
import { colors } from '../../common'
import CalendarStrip from 'react-native-calendar-strip'

export class DatePicker extends Component {
  render() {
    return (
      <CalendarStrip
        calendarHeaderStyle={{ color: colors.accentLight }}
        calendarColor={colors.primary}
        dateNumberStyle={{ color: colors.secondary }}
        dateNameStyle={{ color: colors.secondary }}
        // leftSelector={[]}
        // rightSelector={[]}
        Type="background"
        highlightColor={colors.primary}
        style={{ height: 100, paddingTop: 20, paddingBottom: 10 }}
      />
    )
  }
}
