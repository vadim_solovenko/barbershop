import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native'
import { colors } from '../../common'

export class Time extends Component {
  onPress = value => () => {
    const { onChange } = this.props
    onChange(value)
  }

  render() {
    const { times = [], selected = '', sendOrder } = this.props
    return (
      <View style={styles.container}>
        {times.map(item => (
          <TouchableHighlight
            onPress={this.onPress(item.value)}
            underlayColor={colors.accentLight}
            key={item.value}
            style={{
              borderWidth: !item.free && item.value === selected ? 0 : 1,
              borderRadius: 5,
              borderColor: colors.primary,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 10,
              paddingHorizontal: 14,
              marginHorizontal: 10,
              marginVertical: 10,
              ...(item.value === selected
                ? { backgroundColor: colors.accentLight }
                : {})
            }}
          >
            <Text style={styles.text}>{item.value.substring(0, 5)}</Text>
          </TouchableHighlight>
        ))}
        {
          <View style={{ width: '100%' }}>
            <TouchableOpacity
              onPress={sendOrder}
              style={styles.button}
              activeOpacity={0.8}
            >
              <Text style={styles.buttonText}>Готово</Text>
            </TouchableOpacity>
          </View>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 10,
    justifyContent: 'center',
    paddingBottom: 20
  },
  inner: {},
  text: {
    color: colors.primary,
    fontSize: 18
  },
  button: {
    position: 'relative',
    width: '50%',
    alignSelf: 'center',
    backgroundColor: colors.primary,
    marginTop: 40,
    paddingVertical: 12,
    borderRadius: 2,
    alignItems: 'center'
  },
  buttonText: {
    fontFamily: 'SFUIText-Regular',
    fontSize: 20,
    color: colors.secondary
  }
})
