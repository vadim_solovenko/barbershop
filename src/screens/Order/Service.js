import React, { Component } from 'react'
import { connect } from 'react-redux'
import { select } from '@rematch/select'
import { View, Image, Text, StyleSheet } from 'react-native'
import Carousel, { ParallaxImage } from 'react-native-snap-carousel'
import { colors, width } from '../../common'

export class _Service extends Component {
  _renderItem = ({ item, index }, parallaxProps) => {
    const { imageUrl, name, description, price, duration } = item
    return (
      <View style={styles.item}>
        <ParallaxImage
          source={{ uri: imageUrl }}
          parallaxFactor={0.5}
          style={styles.item}
          containerStyle={styles.item}
          {...parallaxProps}
        />
      </View>
    )
  }

  componentDidMount() {
    const { services, id } = this.props
    const order = services.reduce(
      (acc, item) => (item.id === id ? item.order : acc),
      0
    )

    this._carousel.snapToItem(order, true)
    this._carousel.snapToItem(order)
    console.log(id, 'QQQ', order, 'WWW', this._carousel.currentIndex)
  }

  onSnapToItem = slideIndex => {
    const { onChange, services } = this.props
    const id = services.reduce(
      (acc, item) => (item.order === slideIndex ? item.id : acc),
      undefined
    )
    onChange(id)
  }

  render() {
    const { services } = this.props
    return (
      <Carousel
        ref={c => {
          this._carousel = c
        }}
        data={services}
        renderItem={this._renderItem}
        hasParallaxImages={true}
        sliderWidth={width}
        itemWidth={200}
        loop={true}
        onSnapToItem={this.onSnapToItem}
        inactiveSlideOpacity={0.6}
        inactiveSlideScale={0.9}
      />
    )
  }
}

const styles = StyleSheet.create({
  item: {
    width: 200,
    height: 200,
    borderRadius: 100
  }
})

// const mapState = (state, { initId }) => {
//   const items = Object.values(state.services)
//   const currentOrder = items.reduce(
//     (acc, item, self) =>
//       item.id === state.selected.selectedService ? item.order : acc,
//     0
//   )
//   const services = items
//     .map((item, index, self) => ({
//       ...item,
//       slideIndex:
//         item.order - currentOrder >= 0
//           ? item.order - currentOrder
//           : self.length - currentOrder + item.order
//     }))
//     .sort((a, b) => a.slideIndex > b.slideIndex)
//   console.log(services)
//   return { services }
// }

const mapState = state => ({
  services: select.services.getAllDetail(state)
})

export const Service = connect(mapState)(_Service)
