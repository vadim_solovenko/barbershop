import React from 'react'
import { ActivityIndicator, StatusBar, View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { select } from '@rematch/select'

class _AuthLoading extends React.Component {
  constructor(props) {
    super(props)
    this.props.navigation.navigate(props.userData.email ? 'App' : 'Auth')
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

const mapState = state => ({
  userData: select.auth.userData(state)
})

export const AuthLoading = connect(mapState)(_AuthLoading)
