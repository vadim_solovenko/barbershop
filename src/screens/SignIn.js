import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  View,
  ImageBackground,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  StatusBar
} from 'react-native'
import { width, colors, proportions } from '../common'

class _SignIn extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    email: 'user2',
    password: 'user2pass'
  }

  onChangeEmail = email => this.setState({ email })
  onChangePassword = password => this.setState({ password })

  signIn = async () => {
    const { email, password } = this.state
    const body = JSON.stringify({
      login: email,
      password
    })
    const response = await fetch('http://176.31.32.73:8001/api/User/Login', {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'post',
      body
    })
    const { signIn, navigation } = this.props
    if (response.ok) {
      signIn({ email, password })
      navigation.navigate('App')
    }
  }

  render() {
    return (
      <ImageBackground
        source={require('../../resources/images/barbershop.png')}
        style={styles.background}
        opacity={0.5}
      >
        <StatusBar hidden={true} />
        <View style={styles.container}>
          <View style={styles.innerContainer}>
            <Image source={{ uri: 'logo' }} style={styles.iconLogo} />
          </View>
          <TextInput
            value={this.state.email}
            onChangeText={this.onChangeEmail}
            placeholder="e-mail"
            keyboardType="email-address"
            style={styles.input}
            placeholderTextColor={colors.opacity}
            underlineColorAndroid={colors.opacity}
          />
          <TextInput
            value={this.state.password}
            onChangeText={this.onChangePassword}
            placeholder="пароль"
            secureTextEntry={true}
            style={styles.input}
            placeholderTextColor={colors.opacity}
            underlineColorAndroid={colors.opacity}
          />
          <TouchableOpacity
            onPress={this.signIn}
            style={styles.button}
            activeOpacity={0.8}
          >
            <Text style={[styles.text, { fontSize: 20 }]}>войти</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ alignItems: 'flex-end' }}
            activeOpacity={0.8}
          >
            <Text style={styles.text}>регистрация</Text>
          </TouchableOpacity>
          <View style={styles.innerContainer}>
            <View style={styles.containerSocial}>
              <TouchableOpacity activeOpacity={0.8}>
                <Image
                  source={{ uri: 'icon_twitter' }}
                  style={styles.iconSocial}
                />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8}>
                <Image
                  source={{ uri: 'icon_facebook' }}
                  style={styles.iconSocial}
                />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.8}>
                <Image source={{ uri: 'icon_vk' }} style={styles.iconSocial} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.primary
  },
  container: {
    flex: 1,
    marginHorizontal: '15%',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  innerContainer: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  iconLogo: {
    width: 0.7 * width,
    height: proportions.logo * 0.7 * width
  },
  button: {
    backgroundColor: colors.accent,
    paddingVertical: 12,
    borderRadius: 2,
    alignItems: 'center'
  },
  input: {
    color: 'white',
    paddingHorizontal: 20,
    paddingBottom: 20,
    fontSize: 18
  },
  text: {
    color: 'white'
  },
  containerSocial: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: 0.5 * width
  },
  iconSocial: {
    width: 0.15 * width,
    height: 0.15 * width
  }
})

const mapDispatch = dispatch => ({
  signIn: dispatch.auth.signIn
})

export const SignIn = connect(
  null,
  mapDispatch
)(_SignIn)
