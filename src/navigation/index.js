import { createSwitchNavigator, createStackNavigator } from 'react-navigation'
import { AuthLoading, SignIn, Services, Order } from '../screens'

const App = createStackNavigator(
  { Services, Order },
  {
    navigationOptions: {
      headerStyle: {
        backgroundColor: 'rgba(0,0,0,0)'
      }
      // ,
      // headerTintColor: '#fff',
      // headerTitleStyle: {
      //   fontWeight: 'bold',
      // },
    }
  }
)
const Auth = createStackNavigator({ SignIn })

export const Navigator = createSwitchNavigator(
  {
    AuthLoading,
    App,
    Auth
  },
  {
    initialRouteName: 'AuthLoading'
  }
)
