import { init } from '@rematch/core'
import selectorsPlugin from '@rematch/select'
import createRematchPersist from '@rematch/persist'
import * as models from './models'

export default (initialState = {}) => {
  const persistPlugin = createRematchPersist({
    version: 1
  })
  return init({
    models,
    redux: {
      initialState
    },
    plugins: [persistPlugin, selectorsPlugin()]
  })
}
