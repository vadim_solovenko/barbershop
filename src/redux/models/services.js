export const services = {
  state: {},
  reducers: {
    load(state, data = []) {
      return data.reduce(
        (acc, item, order) => ({ ...acc, [item.id]: { ...item, order } }),
        {}
      )
    }
  },
  selectors: {
    getAll: state =>
      Object.values(state)
        .sort((a, b) => a.order > b.order)
        .map(item => item.id),
    getAllDetail: state => {
      return Object.values(state).sort((a, b) => a.order > b.order)
    },
    getAllDetail1: (state, id) => {
      const items = Object.values(state)
      const currentOrder = items.reduce(
        (acc, item, self) => (item.id === id ? item.order : acc),
        0
      )
      const services = items
        .map((item, index, self) => ({
          ...item,
          slideIndex:
            item.order - currentOrder >= 0
              ? item.order - currentOrder
              : self.length - currentOrder + item.order
        }))
        .sort((a, b) => a.slideIndex > b.slideIndex)
      return services
    },
    getItem: (state, id) => state[id] || {}
  }
}
