export const auth = {
  state: {},
  reducers: {
    signIn(state, { email, password }) {
      return { ...state, email, password }
    },
    signOut(state) {
      return {}
    }
  },
  selectors: { userData: state => state }
}
