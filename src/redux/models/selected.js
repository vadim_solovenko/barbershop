export const selected = {
  state: {},
  reducers: {
    setSelectedService(state, id) {
      return { ...state, selectedService: id }
    }
  },
  selectors: { getService: state => state.selectedService }
}
