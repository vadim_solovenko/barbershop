import { Dimensions, StatusBar } from 'react-native'

export const headerHeight = 55

export const { width, height } = Dimensions.get('window')

export const proportions = {
  logo: 99 / 371,
  servicesBackground: 397 / 363
}

export const colors = {
  primary: '#464646',
  primaryLight: 'rgba(70,70,70,0.7)',
  secondary: 'white',
  accent: '#b09c7b',
  accentLight: '#c3bc9c',
  opacity: 'rgba(255,255,255,0.5)'
}

export const getMinutes = time => {
  try {
    const timeParts = time.split(':').map(item => Number(item))
    return 60 * timeParts[0] + timeParts[1]
  } catch (err) {
    return time
  }
}
