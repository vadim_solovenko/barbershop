import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Navigator } from './src/navigation'
import configureStore from './src/redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import { getPersistor } from '@rematch/persist'
import { ActivityIndicator } from 'react-native'

const store = configureStore()

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<ActivityIndicator />} persistor={getPersistor()}>
          <Navigator />
        </PersistGate>
      </Provider>
    )
  }
}
